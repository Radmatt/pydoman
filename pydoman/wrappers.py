"""For any and all magic methods, return this. Exceptions being equality comparison."""

import traceback
import builtins

__exceptional_record__ = []

def monadic(function, *args, **kwargs):
    """Allow functions to use Monads to avoid exceptions and null (None) pointer errors,
    but just use regular python function calls, not fmap or amap.

    If the function is given a parameter that is an instance of Nothing or Left,
    return the first such parameter instead of calling the function.
    Also, if the function does not return a Container instance, wrap the return
    value in a Result object, unless it is None and so return Nothing instead.
    """

    # IF...you wrap values in Active objects, they can just check the values when used e.g.
    # just wrap the list and decorate __getitem__ for Active objects with the monadic function. If
    # the function returns a passive, there you go. No need to check the whole list. OR IS THERE?!
    checked_args = []
    for i in range(0, builtins.len(args)):
        passive, checked_arg = _recursive_passive_check(args[i], False)
        if passive is not None:
            return passive
        else:
            checked_args.append(checked_arg)
    checked_kwargs = {}
    for key in kwargs:
        passive, checked_kwarg = _recursive_passive_check(kwargs[key], False)
        if passive is not None:
            return passive
        checked_kwargs[key] = checked_kwarg
    try:
        result = function(*checked_args, **checked_kwargs)
    except Exception as e:
        return Exceptional(e)
    else:
        passive, return_val = _recursive_passive_check(result)
        if passive is not None:
            return passive
        else:
            return _wrap_variable(return_val)

base_types = builtins.map(builtins.type, [
    0,
    0.0,
    0j,
    '',
    u'',
    False,
])


def _recursive_passive_check(variable, make_active=True):
    if builtins.isinstance(variable, Passive):
        return variable, None
    if builtins.isinstance(variable, Exception):
        return Exceptional(variable), None
    if variable is None:
        return Nothing, None
    if builtins.type(variable) in base_types:
        return None, variable
    if variable == {}:
        return None, variable
    if variable == []:
        return None, variable
    try:
        for key, value in variable.iteritems():
            passive, checked = _recursive_passive_check(value)
            if passive is not None:
                return passive, None
        return None, variable
    except Exception:
        pass
    try:
        for elem in variable:
            passive, checked = _recursive_passive_check(elem)
            if passive is not None:
                return passive, None
        return None, _wrap_variable(variable, make_active)
    except Exception:
        return None, _wrap_variable(variable, make_active)




def _wrap_variable(variable, make_active=True):
    # Wrapping dictionaries and lists could really slow things down. Either give the user the choice,
    # or just limit the scope of this repo to the code the user writes themself.
    # ACTUALLY, Active object wrappers can work - the __get__  and __repr__ methods are key to using them transparently
    if builtins.isinstance(variable, Exception):
        return Exceptional(variable)
    elif variable is None:
        return Nothing
    elif builtins.isinstance(variable, Passive):
        return variable
    if make_active:
        if builtins.isinstance(variable, Active):
            return variable
        else:
            return Active(variable)
    else:
        if builtins.isinstance(variable, Active):
            return variable.value
        else:
            return variable

class Traceable(object):
    # DEVNOTE: A top-level class i.e. inherits from object ONLY
    """The same as `Left`, but includes traceback information."""

    
    def __init__(self, error_message):
        # Set the traceback and trim the call to this function
        self.error = error_message
        self.traceback = traceback.format_stack()[:-1]

    
    def __str__(self):
        return '%s%s' % (''.join(self.traceback), self.error)

class Active(object):
    # DEVNOTE: A top-level class i.e. inherits from object ONLY
    """Implements all magic methods in python, but almost always just returns the object."""

    def __new__(cls, value):
        if builtins.isinstance(value, Passive):
            return value
        if builtins.isinstance(value, Active):
            return value
        return super(Active, cls).__new__(cls)
    
    def __init__(self, value):
        if builtins.isinstance(value, Passive):
            self = value
        if builtins.isinstance(value, Active):
            pass
        else:
            self._value = value

    def __getattribute__(self, name):
        if name == 'value' or name == '_value':
            return object.__getattribute__(self, '_value')
        else:
            try:
                return object.__getattribute__(self._value, name)
            except AttributeError:
                pass
        try:
            return object.__getattribute__(self, name)
        except AttributeError as e:
            return Exceptional(e)

    def __str__(self, *args, **kwargs):
        return builtins.str(self.value)

    def __abs__(self, *args, **kwargs):
        return monadic(abs, self.value, *args, **kwargs)

    def __call__(self, *args, **kwargs):
        try:
            return monadic(self._value.__call__, *args, **kwargs)
        except Exception as e:
            return Exceptional(e)

    def __coerce__(self, other):
        result = monadic(builtins.coerce, self._value, _wrap_variable(other)._value)
        if result is self:
            return self
        if result is other:
            return other
        if not result:
            return Nothing
        else:
            return result.value

    def __complex__(self, *args, **kwargs):
        return monadic(builtins.complex, self._value)

    def __contains__(self, *args, **kwargs):
        try:
            result = monadic(self._value.__contains__, *args, **kwargs)
        except AttributeError as e:
            return Exceptional(e)
        if builtins.isinstance(result, Exceptional):
            return result
        else:
            return result.value

    def __delitem__(self, *args, **kwargs):
        try:
            monadic(self._value.__delitem__, *args, **kwargs)
        except AttributeError as e:
            return Exceptional(e)
    def __del__(self, *args, **kwargs):
        try:
            monadic(self._value.__del__, *args, **kwargs)
        except AttributeError as e:
            return Exceptional(e)
# Checked all above this line
#     
#     
# 
#     
# 
#     
#    def __delattr__(self, *args, **kwargs):
#         return self
# 
#     
#    def __delete__(self, *args, **kwargs):
#         return self
# 
#     
# 
#     
#    def __delslice__(self, *args, **kwargs):
#         return self
# 
#     
#    def __dict__(self, *args, **kwargs):
#         return self
# 
#     
#    def __div__(self, *args, **kwargs):
#         return self
# 
#     
#    def __divmod__(self, *args, **kwargs):
#         return self
# 
#     
#    def __doc__(self, *args, **kwargs):
#         return self
# 
#     
#    def __enter__(self, *args, **kwargs):
#         return self
# 
#     
#    def __exit__(self, *args, **kwargs):
#         return self
# 
#     
#    def __file__(self, *args, **kwargs):
#         return self
# 
#     
#    def __float__(self, *args, **kwargs):
#         return self
# 
#     
#    def __floordiv__(self, *args, **kwargs):
#         return self
# 
#     
#    def __future__(self, *args, **kwargs):
#         return self
# 
#     
    def __get__(self, *args, **kwargs):
        return self.value
# 
#     
#    def __getattr__(self, *args, **kwargs):
#         return self
# 
#     
#    def __getattribute__(self, *args, **kwargs):
#         return self
# 
#     
#    def __getitem__(self, *args, **kwargs):
#         return self
# 
#     
#    def __getslice__(self, *args, **kwargs):
#         return self
# 
#     
#    def __globals__(self, *args, **kwargs):
#         return self
# 
#     
#    def __hash__(self, *args, **kwargs):
#         return self
# 
#     
#    def __hex__(self, *args, **kwargs):
#         return self
# 
#     
#    def __iadd__(self, *args, **kwargs):
#         return self
# 
#     
#    def __iand__(self, *args, **kwargs):
#         return self
# 
#     
#    def __idiv__(self, *args, **kwargs):
#         return self
# 
#     
#    def __ifloordiv__(self, *args, **kwargs):
#         return self
# 
#     
#    def __ilshift__(self, *args, **kwargs):
#         return self
# 
#     
#    def __imod__(self, *args, **kwargs):
#         return self
# 
#     
#    def __imul__(self, *args, **kwargs):
#         return self
# 
#     
#    def __index__(self, *args, **kwargs):
#         return self
# 
#     
#    def __instancecheck__(self, *args, **kwargs):
#         return self
# 
#     
    def __int__(self, *args, **kwargs):
        try:
            return self.value.__int__()
        except Exception as e:
            return Exceptional(e)
# 
#     
#    def __invert__(self, *args, **kwargs):
#         return self
# 
#     
#    def __ior__(self, *args, **kwargs):
#         return self
# 
#     
#    def __ipow__(self, *args, **kwargs):
#         return self
# 
#     
#    def __irshift__(self, *args, **kwargs):
#         return self
# 
#     
#    def __isub__(self, *args, **kwargs):
#         return self
# 
#     
#    def __iter__(self, *args, **kwargs):
#         yield self
# 
#     
#    def __itruediv__(self, *args, **kwargs):
#         return self
# 
#     
#    def __ixor__(self, *args, **kwargs):
#         return self
# 
#     
#    def __len__(self, *args, **kwargs):
#         return 0
# 
#     
#    def __long__(self, *args, **kwargs):
#         return self
# 
#     
#    def __lshift__(self, *args, **kwargs):
#         return self
# 
#     
#    def __metaclass__(self, *args, **kwargs):
#         return self
# 
#     
#    def __missing__(self, *args, **kwargs):
#         return self
# 
#     
#    def __mod__(self, *args, **kwargs):
#         return self
# 
#     
#    def __module__(self, *args, **kwargs):
#         return self
# 
#     
#    def __mul__(self, *args, **kwargs):
#         return self
# 
#     
#    def __name__(self, *args, **kwargs):
#         return self
# 
#     
#    def __new__(self, *args, **kwargs):
#         return self
# 
#     
#    def __oct__(self, *args, **kwargs):
#         return self
# 
#     
#    def __op__(self, *args, **kwargs):
#         return self
# 
#     
#    def __pos__(self, *args, **kwargs):
#         return self
# 
#     
#    def __pow__(self, *args, **kwargs):
#         return self
# 
#     
#    def __radd__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rand__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rcmp__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rdiv__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rdivmod__(self, *args, **kwargs):
#         return self
# 
#     
#    def __repr__(self, *args, **kwargs):
#         return self
# 
#     
#    def __reversed__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rfloordiv__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rlshift__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rmod__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rmul__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rop__(self, *args, **kwargs):
#         return self
# 
#     
#    def __ror__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rpow__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rrshift__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rshift__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rsub__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rtruediv__(self, *args, **kwargs):
#         return self
# 
#     
#    def __rxor__(self, *args, **kwargs):
#         return self
# 
#     
#    def __self__(self, *args, **kwargs):
#         return self
# 
#     
#    def __set__(self, *args, **kwargs):
#         return self
# 
#     
#    def __setattr__(self, *args, **kwargs):
#         return self
# 
#     
#    def __setitem__(self, *args, **kwargs):
#         return self
# 
#     
#    def __setslice__(self, *args, **kwargs):
#         return self
# 
#     
#    def __slots__(self, *args, **kwargs):
#         return self
# 
#     
# 
#     
#    def __sub__(self, *args, **kwargs):
#         return self
# 
#     
#    def __subclasscheck__(self, *args, **kwargs):
#         return self
# 
#     
#    def __truediv__(self, *args, **kwargs):
#         return self
# 
#     
#    def __unicode__(self, *args, **kwargs):
#         return self
# 
#     
#    def __weakref__(self, *args, **kwargs):
#         return self
# 
# # LOGICAL #
# 
#     
#    def __bool__(self, *args, **kwargs):
#         return self
# 
#     
#    def __xor__(self, *args, **kwargs):
#         return self
# 
#     
#    def __add__(self, *args, **kwargs):
#         return self
# 
#     
#    def __and__(self, *args, **kwargs):
#         return self
# 
#     
#    def __cmp__(self, *args, **kwargs):
#         return self
# 
#     
    def __eq__(self, other):
        return self.value == _wrap_variable(other).value
# 
#     
#    def __ge__(self, *args, **kwargs):
#         return self
# 
#     
#    def __gt__(self, *args, **kwargs):
#         return self
# 
#     
#    def __le__(self, *args, **kwargs):
#         return self
# 
#     
#    def __lt__(self, *args, **kwargs):
#         return self
# 
#     
#    def __ne__(self, *args, **kwargs):
#         return self
# 
#     
#    def __neg__(self, *args, **kwargs):
#         return self
# 
#     
#    def __or__(self, *args, **kwargs):
#         return self


class Passive(object):
    # DEVNOTE: A top-level class i.e. inherits from object ONLY
    """Implements all magic methods in python, but almost always just returns the object."""
    # All methods accept *args and **kwargs: whatever the user does to this object, it will return itself.



    def __init__(self, message):
        object.__setattr__(self, '_message', message)

    @property
    def message(self):
        return self._message



    def __str__(self, *args, **kwargs):
        return str(self._message)



    def __abs__(self, *args, **kwargs):
        return self



    def __call__(self, *args, **kwargs):
        return self



    def __contains__(self, *args, **kwargs):
        return False

    @property
    def __defaults__(self):
        return self



    def __delattr__(self, *arg, **kwargs):
        return self



    def __delitem__(self, *arg, **kwargs):
        return self

    def __del__(self, *args, **kwargs):
        if isinstance(self, Exceptional):
            return self
        else:
            del self

    @property
    def __dict__(self):
        return self



    def __getitem__(self, *arg, **kwargs):
        return self



    def __setitem__(self, *arg, **kwargs):
        return self



    def __div__(self, *arg, **kwargs):
        return self



    def __rdiv__(self, *arg, **kwargs):
        return self



    def __divmod__(self, *args, **kwargs):
        return self



    def __rdivmod__(self, *args, **kwargs):
        return self



    def __enter__(self, *args, **kwargs):
        return self



    def __exit__(self, *args, **kwargs):
        return self



    def __float__(self, *args, **kwargs):
        return 0.0



    def __floordiv__(self, *args, **kwargs):
        return self



    def __rfloordiv__(self, *args, **kwargs):
        return self



    def __getattr__(self, *args, **kwargs):
        return self



    def __hash__(self, *args, **kwargs):
        return self
        return 0



    def __hex__(self, *args, **kwargs):
        return self
        return ''



    def __iadd__(self, *args, **kwargs):
        return self



    def __iand__(self, *args, **kwargs):
        return self



    def __idiv__(self, *args, **kwargs):
        return self



    def __ifloordiv__(self, *args, **kwargs):
        return self



    def __ilshift__(self, *args, **kwargs):
        return self



    def __imod__(self, *args, **kwargs):
        return self



    def __imul__(self, *args, **kwargs):
        return self



    def __ior__(self, *args, **kwargs):
        return self



    def __ipow__(self, *args, **kwargs):
        return self



    def __irshift__(self, *args, **kwargs):
        return self



    def __isub__(self, *args, **kwargs):
        return self



    def __ixor__(self, *args, **kwargs):
        return self



    def __index__(self, *args, **kwargs):
        """This will likely throw an exception, but it has to - using None as an index would,
        so if we can't override __getitem__ for builtin lists then this passive object would get
        lost."""
        return self



    def __int__(self, *args, **kwargs):
        return self
        return 0


    def __invert__(self, *args, **kwargs):
        return self


    def __lshift__(self, *args, **kwargs):
        return self


    def __mod__(self, *args, **kwargs):
        return self


    def __mul__(self, *args, **kwargs):
        return self


    def __pos__(self, *args, **kwargs):
        return self


    def __pow__(self, *args, **kwargs):
        return self


    def __add__(self, *args, **kwargs):
        return self


    def __radd__(self, *args, **kwargs):
        return self


    def __and__(self, *args, **kwargs):
        return self


    def __rand__(self, *args, **kwargs):
        return self


    def __lt__(self, *args, **kwargs):
        return self



    def __gt__(self, *args, **kwargs):
        return self


    def __ge__(self, *args, **kwargs):
        return self


    def __le__(self, *args, **kwargs):
        return self



    def __repr__(self, *args, **kwargs):
        return repr(self._message).value


    def __reversed__(self, *args, **kwargs):
        return self


    def __rlshift__(self, *args, **kwargs):
        return self


    def __rmod__(self, *args, **kwargs):
        return self


    def __rmul__(self, *args, **kwargs):
        return self


    def __or__(self, *args, **kwargs):
        return self


    def __ror__(self, *args, **kwargs):
        return self


    def __rpow__(self, *args, **kwargs):
        return self


    def __rrshift__(self, *args, **kwargs):
        return self


    def __rshift__(self, *args, **kwargs):
        return self


    def __sub__(self, *args, **kwargs):
        return self


    def __rsub__(self, *args, **kwargs):
        return self


    def __xor__(self, *args, **kwargs):
        return self


    def __rxor__(self, *args, **kwargs):
        return self

    def __instancecheck__(self, *args, **kwargs):
        return self


    def __bool__(self, *args, **kwargs):
        """Easily check if this is passive using an if statement."""
        return False


    def __eq__(self, *args, **kwargs):
        return self


    def __ne__(self, *args, **kwargs):
        return self


    def __neg__(self, *args, **kwargs):
        return self
# Checked all above this line


    def __get__(self, *args, **kwargs):
        return self


    def __delslice__(self, *args, **kwargs):
        return self


    def __getslice__(self, *args, **kwargs):
        return self


    def __setslice__(self, *args, **kwargs):
        return self

#     
#     def __iter__(self):
#         return self
# 
#     
#     def next(self):
#         return self
# 
#     
#     def __next__(self):
#         return self

    
    def __itruediv__(self, *args, **kwargs):
        return self

    
    def __truediv__(self, *args, **kwargs):
        return self

    
    def __rtruediv__(self, *args, **kwargs):
        return self

    
    def __setattr__(self, *args, **kwargs):
        return self

    
    def __set__(self, *args, **kwargs):
        return self


Nothing = Passive('None')

class Exceptional(Passive, Traceable):
    # DEVNOTE: A composite class i.e. inherits >=2 top-level classes, does nothing but call the
    # __init__ of its parents in its own __init__
    """The same as `Left`, but includes traceback information."""

    
    def __init__(self, exception):
        Passive.__init__(self, '%s: %s' % (exception.__class__.__name__, exception))
        Traceable.__init__(self, '%s: %s' % (exception.__class__.__name__, exception))


class Failure(Passive):
    # An alias - just a nicer name for Passive to make your code more readable

    
    def __init__(self, message):
        Passive.__init__(self, message)


def isinstance(the_object, the_class):
    if builtins.isinstance(the_object, Passive):
        return builtins.isinstance(the_object, the_class)
    return monadic(builtins.isinstance, the_object, the_class)


def float(*args, **kwargs):
    return monadic(builtins.float, *args, **kwargs)


def complex(*args, **kwargs):
    return monadic(builtins.complex, *args, **kwargs)


def str(*args, **kwargs):
    return monadic(builtins.str, *args, **kwargs)


def int(*args, **kwargs):
    return monadic(builtins.int, *args, **kwargs)


def len(*args, **kwargs):
    return monadic(builtins.len, *args, **kwargs)


def oct(*args, **kwargs):
    return monadic(builtins.oct, *args, **kwargs)


def repr(*args, **kwargs):
    return monadic(builtins.repr, *args, **kwargs)


def bool(*args, **kwargs):
    return monadic(builtins.bool, *args, **kwargs)


def hex(*args, **kwargs):
    return monadic(builtins.hex, *args, **kwargs)


def set(*args, **kwargs):
    return monadic(builtins.set, *args, **kwargs)


def hash(*args, **kwargs):
    return monadic(builtins.hash, *args, **kwargs)


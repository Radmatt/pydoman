from distutils.core import setup

setup(
    name='Passify',
    version='0.1',
    author='Matthew Rademaker',
    author_email='matthew@acctv.com.au',
    packages=['passify', 'tests'],
    url='https://bitbucket.org/radmatt/passify',
    license=open('LICENSE.txt').read(),
    description='Something similar to pymonad, but allows for more pythonic-looking code. The main purpose is to avoid raising exceptions and None reference errors.',
    long_description=open('README.md').read() + open("CHANGES.md").read(),
        classifiers=[ "Intended Audience :: Developers"
                                #, "License :: OSI Approved :: BSD License"
                                , "Operating System :: OS Independent"
                                , "Programming Language :: Python :: 2.7"
                                , "Programming Language :: Python :: 3"
                                , "Topic :: Software Development"
                                , "Topic :: Software Development :: Libraries"
                                , "Topic :: Utilities"
                                ],
)

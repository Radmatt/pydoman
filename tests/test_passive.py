# To test __truediv__, __rtruediv__, __itruediv__, uncomment the following line
# from __future__ import division
import sys
import unittest
import pydoman as pd
import builtins
from pydoman import *
from decimal import Decimal


class TestObject(object):

    def __init__(self):
        self.a = 1


class TestPassive(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):
        # TODO: All methods and functions that python requires a particular return type for should have a wrapper
        # that the user can use instead, which for passive objects returns it, for active calls the
        # builtin
        # __init__
        try:
            pd.Passive()
        except Exception:
            assert True
        else:
            assert False
        pd.Passive(None)
        test_object = pd.Passive('test')
        # __str___
        assert str(test_object) is test_object
        # __abs___
        assert abs(test_object) is test_object
        # __call___
        assert test_object() is test_object
        assert test_object(1) is test_object
        assert test_object(a=1) is test_object
        # __complex___
        assert complex(test_object) is test_object
        # __contains__
        test_object = pd.Passive(['a', 1])
        assert 'a' not in test_object
        assert 1 not in test_object
        # __defaults__
        assert test_object.__defaults__ is test_object
        # __del__
        test_object = pd.Passive('test')
        del test_object
        try:
            test_object
        except Exception:
            assert True
        else:
            assert False
        test_exceptional = pd.Exceptional('test')
        del test_exceptional
        try:
            test_exceptional
        except Exception:
            assert True
        else:
            assert False
        # __delattr__
        test_object = pd.Passive('test')
        del test_object.muskrat
        # __delitem__
        del test_object[0]
        del test_object[0:1]
        del test_object[0:2:-1]
        del test_object['key']
        # __dict__
        test_object.__dict__
        test_object.__dict__['_message'] = 'hi'
        assert test_object.message is 'test'
        # __setitem__
        test_object[0] = 'hi'
        assert test_object[0] is not 'hi'
        # __div__, __rdiv__
        test_object = pd.Passive(1)
        assert test_object / 2 is test_object
        assert 2 / test_object is test_object
        # __divmod__
        assert divmod(test_object, 2) is test_object
        assert divmod(2, test_object) is test_object
        # __doc__
        assert test_object.__doc__ is pd.Passive.__doc__
        # __enter__, __exit__
        with test_object as ttest:
            assert ttest is test_object
        # __float__
        assert float(test_object) is test_object
        # __floordiv__, __rfloordiv__
        assert test_object // 2 is test_object
        assert 2 // test_object is test_object
        # __getattr__
        assert test_object.__getattr__('blah') is test_object
        assert test_object.a_thing is test_object
        # __getitem__
        assert test_object['a_thing'] is test_object
        assert test_object[0] is test_object
        assert test_object[0:1] is test_object
        assert test_object[3:99] is test_object
        # __hash__
        assert hash(test_object) is test_object
        assert set(test_object) is test_object
        test_object2 = pd.Passive(2)
        assert set([test_object, test_object2]) is test_object
        assert set([1, test_object]) is test_object
        # __hex__
        assert hex(test_object) is test_object
        # __iadd__
        saved_test_object = str(test_object)
        test_object += 1
        assert str(test_object) is saved_test_object
        # __iand__
        test_object &= True
        assert str(test_object) is saved_test_object
        # __idiv__
        test_object /= 2
        assert str(test_object) is saved_test_object
        # __ifloordiv
        test_object //= 2
        assert str(test_object) is saved_test_object
        # __ilshift__
        test_object <<= 2
        assert str(test_object) is saved_test_object
        # __imod__
        test_object %= 2
        assert str(test_object) is saved_test_object
        # __imul__
        test_object *= 2
        assert str(test_object) is saved_test_object
        # __ior__
        test_object |= True
        assert str(test_object) is saved_test_object
        # __ipow__
        test_object **= 2
        assert str(test_object) is saved_test_object
        # __irshift__
        test_object >>= 2
        assert str(test_object) is saved_test_object
        # __isub__
        test_object -= 1
        assert str(test_object) is saved_test_object
        # __ixor__
        test_object ^= True
        assert str(test_object) is saved_test_object
        # __index__
        test_list = [1, 2, 3]
        try:
            test_list[test_object]
        except Exception:
            assert True
        else:
            assert False
        # __int__
        assert int(test_object) is test_object
        # __invert__
        assert ~test_object is test_object
        # __len__
        assert len(test_object) is test_object
        # __lshift__, __rlshift__
        assert test_object << 2 is test_object
        assert 2 << test_object is test_object
        # __mod__, __rmod__
        assert test_object % 2 is test_object
        assert 2 % test_object is test_object
        # __mul__, __rmul__
        assert test_object * 2 is test_object
        assert 2 * test_object is test_object
        # __oct__
        assert oct(test_object) is test_object
        # __pos__
        assert +test_object is test_object
        # __pow__, __rpow__
        assert pow(test_object, 2) is test_object
        assert test_object ** 2 is test_object
        assert pow(2, test_object) is test_object
        assert 2 ** test_object is test_object
        # __add__, __radd__
        assert test_object + 2 is test_object
        assert 2 + test_object is test_object
        # __and__, __rand__
        assert test_object & True is test_object
        assert True & test_object is test_object
        # __lt__
        assert (test_object < 2) is test_object
        assert (test_object <= 2) is test_object
        # __gt__
        assert (test_object > 2) is test_object
        assert (test_object >= 2) is test_object
        # __repr__
        assert repr(test_object) is test_object
        # __reversed__
        assert reversed(test_object) is test_object
        # __or__, __ror__
        assert test_object | False is test_object
        assert False | test_object is test_object
        # __eq__
        assert (test_object == 2) is test_object
        assert (2 == test_object) is test_object
        # __rshift__, __rrshift__
        assert test_object >> 2 is test_object
        assert 2 >> test_object is test_object
        # __sub__, __rsub__
        assert test_object - 2 is test_object
        assert 2 - test_object is test_object
        # __xor__, __rxor__
        assert test_object ^ True is test_object
        assert True ^ test_object is test_object
        # __instancecheck__
        assert not isinstance(test_object, builtins.int)
        assert isinstance(test_object, pd.Passive)
        # __nonzero__
        # we want to be able to say 'if test_object:' to test if active or passive
        if test_object:
            assert False
        else:
            assert True
        assert bool(test_object) is test_object
        # __ne__
        assert (test_object != 2) is test_object
        assert (2 != test_object) is test_object
        # __neg__
        assert -test_object is test_object
        # __assign__
        import pudb; pu.db
        saved_test_object = str(test_object)
        test_object = 2
        assert saved_test_object == str(test_object)


class TestActive(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):
        # TODO: All methods and functions that python requires a particular return type for should have a wrapper
        # that the user can use instead, which for passive objects returns it, for active calls the
        # builtin
        # __init__
        try:
            pd.Active()
        except Exception:
            assert True
        else:
            assert False
        pd.Active(None)
        test_object = pd.Active(1)
        # __str___
        # !! Need to unwrap or wrap the args in the monadic function
        assert str(test_object) == str(test_object.value)
        # __abs___
        test_object = pd.Active('hi')
        assert isinstance(abs(test_object), Exceptional)
        test_object = pd.Active(1)
        assert abs(test_object) == abs(test_object.value)
        # __call___

        def a_function(a, b):
            return 3
        test_object = pd.Active(a_function)
        assert isinstance(test_object(2), Exceptional)
        assert isinstance(test_object(), Exceptional)
        assert test_object('a', []) == 3
        # __complex___
        test_object = 1
        assert complex(test_object).value == builtins.complex(1)
        assert complex(test_object).value == builtins.complex(test_object)
        test_object = 'blah'
        assert isinstance(complex(test_object), Exceptional)
        test_object = pd.Active(1)
        assert complex(test_object).value == builtins.complex(1)
        assert complex(test_object).value == builtins.complex(test_object.value)
        test_object = pd.Active('blah')
        assert isinstance(complex(test_object), Exceptional)
        # __contains__
        test_object = pd.Active(['a', 1])
        assert 'a' in test_object
        assert 1 in test_object
        assert 2 not in test_object
        test_object = pd.Active(1)
        assert isinstance(1 in test_object, Exceptional)
        # __defaults__
        assert isinstance(test_object.__defaults__, Exceptional)

        def x(y, z=False):
            return y
        test_object = pd.Active(x)
        assert test_object.__defaults__ is test_object.value.__defaults__
        # __del__
        test_object = pd.Active(1)
        del test_object
        try:
            test_object
        except Exception:
            assert True
        # __delattr__
        # __delitem__
        test_object = pd.Active({'a': 2})
        del test_object['a']
        assert test_object.value == {}
        # __dict__
        test_dummy = TestObject()
        test_object = pd.Active(test_dummy)
        assert test_object.__dict__ == test_dummy.__dict__
        # __setitem__
        # __div__, __rdiv__
        # __divmod__
        # __doc__
        # __enter__, __exit__
        # __float__
        # __floordiv__, __rfloordiv__
        # __getattr__
        # __getitem__
        # __hash__
        # __hex__
        # __iadd__
        # __iand__
        # __idiv__
        # __ifloordiv
        # __ilshift__
        # __imod__
        # __imul__
        # __ior__
        # __ipow__
        # __irshift__
        # __isub__
        # __ixor__
        # __index__
        # __int__
        # __invert__
        # __len__
        # __long__
        # __lshift__, __rlshift__
        # __mod__, __rmod__
        # __mul__, __rmul__
        # __oct__
        # __pos__
        # __pow__, __rpow__
        # __add__, __radd__
        # __and__, __rand__
        # __lt__
        # __gt__
        # __repr__
        # __reversed__
        # __or__, __ror__
        # __eq__
        # __rshift__, __rrshift__
        # __sub__, __rsub__
        # __xor__, __rxor__
        # __instancecheck__
        # __unicode__
        # __nonzero__
        # __ne__
        # __neg__
        # __assign__


if __name__ is "__main__":
    unittest.main()

